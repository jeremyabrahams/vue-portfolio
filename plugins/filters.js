import Vue from 'vue'

Vue.filter('slugify', val => val.toLowerCase().replace(/ +/g, '-'))
