#  Vue + Nuxt + Netlify + Contentful w/ Webhook based Portfolio


https://vue-portfolio.jeremyabrahams.com

This site uses:
* Vue & Nuxt as a static site generator
* Contentful as a headless CMS 
* Netlify to host the static site

Even though this is a static site the content is still authored in and populated via Contentful's CMS. Using webhooks I'm able to regenerate the static site each time a post is added/updated in Contentful.

  

##  Build Setup

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start
 
# generate static project
$ npm run generate
```
For detailed explanation on how things work, checkout the \[Nuxt.js docs\](https://github.com/nuxt/nuxt.js).
